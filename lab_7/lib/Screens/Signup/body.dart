import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:lab_7/Components/background.dart';
import 'package:lab_7/Components/rounded_button.dart';
import 'package:lab_7/Components/rounded_input_field.dart';
import 'package:lab_7/Components/rounded_password_field.dart';
import 'package:lab_7/Components/title_form.dart';

class Body extends StatelessWidget {
  const Body({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return AnnotatedRegion<SystemUiOverlayStyle> (
      value: SystemUiOverlayStyle.light,
      child: GestureDetector(
        child: Stack(
          children: <Widget>[
            Background(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  TitleForm(text: 'SignUp Form'),
                  SizedBox(height: 50,),
                  RoundedInputField(text: 'Username', inputType: TextInputType.name),
                  SizedBox(height: 20,),
                  RoundedInputField(text: 'Email', inputType: TextInputType.emailAddress),
                  SizedBox(height: 20,),
                  RoundedPasswordField(text: 'Password'),
                  RoundedButton(text: 'SIGNUP'),
                ]
              ),
            ),
          ],
        )
      )
    );
  }
}