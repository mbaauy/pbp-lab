import 'package:flutter/material.dart';

class RoundedPasswordField extends StatelessWidget {
  final String text;
  final Color color, textColor;
  const RoundedPasswordField({
    Key? key,
    required this.text,
    this.color = const Color(0xffA78BFA),
    this.textColor = Colors.white,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          text,
          style: TextStyle(
            color: Colors.black,
            fontSize: 16,
            fontWeight: FontWeight.normal
          ),
        ),
        SizedBox(height: 10),
        Container(
          alignment: Alignment.centerLeft,
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(25),
            boxShadow: [
              BoxShadow(
                color: Colors.black26,
                blurRadius: 3,
                offset: Offset(0,2)
              )
            ]
          ),
          height: 55,
          child: TextField(
            obscureText: true,
            style: TextStyle(
              color: Colors.black87
            ),
            decoration: InputDecoration(
              border: InputBorder.none,
              contentPadding: EdgeInsets.only(top: 1, left: 10),
              hintText: 'Password',
              hintStyle: TextStyle(
                color: Colors.black38,
              )
            ),
          ),
        )
      ]
    );
  }
}