import 'package:flutter/material.dart';

class TitleForm extends StatelessWidget {
  final String text;
  final Color textColor;
  const TitleForm({
    Key? key,
    required this.text,
    this.textColor = const Color(0xffA78BFA),
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      style: TextStyle(
        color: textColor,
        fontSize: 40,
        fontWeight: FontWeight.bold
      ),
    );
  }
}