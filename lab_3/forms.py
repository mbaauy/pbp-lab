from django import forms
from django.forms.fields import DateField
from lab_1.models import Friend


class DateInput(forms.DateInput):
    input_type = 'date'


class FriendForm(forms.ModelForm):
    class Meta:
        model = Friend
        fields = '__all__'
        labels = {
            'name': 'Nama',
            'npm': 'NPM',
            'dob': 'Tanggal Lahir',
        }
        widgets = {
            'dob': DateInput
        }