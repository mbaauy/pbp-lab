from django.http.response import HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from lab_3.forms import FriendForm
from django.shortcuts import render
from lab_1.models import Friend


@login_required(login_url='/admin/login/')
def index(request):
    friends = Friend.objects.all()
    response = {'friends': friends}
    return render(request, 'lab3_index.html', response)

@login_required(login_url='/admin/login/')
def add_friend(request):
    form = FriendForm(request.POST or None)

    if (form.is_valid() and request.method == 'POST'):
        form.save()
        return HttpResponseRedirect("/lab-3/")

    context = {
        'form': form
    }

    return render(request, 'lab3_form.html', context)