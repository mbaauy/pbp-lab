from django.shortcuts import render
from django.http import HttpResponseRedirect
from lab_2.models import Note
from django.shortcuts import render
from lab_4.forms import NoteForm


# Create your views here.
def index(request):
    notes = Note.objects.all()
    response = {'notes': notes}
    return render(request, 'lab4_index.html', response)


def add_note(request):
    note = NoteForm(request.POST or None)

    if (note.is_valid() and request.method == 'POST'):
        note.save()
        return HttpResponseRedirect("/lab-4/")

    context = {
        'note': note
    }

    return render(request, 'lab4_form.html', context)


def note_list(request):
    notes = Note.objects.all()
    response = {'notes': notes}
    return render(request, 'lab4_note_list.html', response)
