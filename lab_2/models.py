from django.db import models

# Create your models here.
class Note(models.Model):
    untuk = models.CharField(max_length=255)
    dari = models.CharField(max_length=255)
    judul = models.CharField(max_length=255)
    pesan = models.TextField(default='')